package com.fineaiops.gateway.controller;

import com.fineaiops.gateway.service.ExpService;
import com.fineaiops.gateway.service.NodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

@RestController
public class DataController {
    private final Logger LOG = LoggerFactory.getLogger(getClass());
    @Autowired
    private NodeService nodeService;
    @Autowired
    private ExpService elasticService;

    @RequestMapping(value = "/trace")
    public String convert(){
        LOG.info("trace dump start");
        nodeService.convertTrace();
        LOG.info("trace dump finish");
        nodeService.syncNode();
        return "success";
    }

    @RequestMapping(value = "/save")
    public String saveCSVtoMysql() {
        {
            try {
                //  读取文件 “GBK”解决文件里中文乱码
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("/Users/jialichun/本地文稿/dataset/training_data_with_faults/tar/cloudbed-1/trace/all/trace_jaeger-span.csv"), "GBK"));
                reader.readLine();//第一行信息是标题信息，跳过，如果需要第一行信息，请删除这行代码
                String line ;

                // 定义list，方便后面批次新增 TlVacuumFurnaceLog是表实体类
                int i = 0;
                int min = 5898270;
                int max = 10000000;
                for (;i < min; i++) {
                    reader.readLine();
                }
                for (;(line = reader.readLine()) != null && i < max; i++) {
                    System.out.println("start: " + i);
                    String item[] = line.split(",");
                    if (item.length < 9) {
                        item = Arrays.copyOf(item, 9);
                    }
                    nodeService.saveTraceInfo(item[0], item[1].split("-")[0], item[2], item[3], item[4], item[5], item[6], item[7],item[8]);
                }
            } catch(Exception e){
                e.printStackTrace();
                return "fail";
            }
        }
        return "success";
    }
}


