package com.fineaiops.gateway.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;

public class LogExpEntity {
    @JSONField(name="id")
    private String id;
    @JSONField(name="content")
    private String content;
    @JSONField(name="service_id")
    private String serviceID;
    @JSONField(name="status")
    private String status;
    @JSONField(name="log_ids")
    private ArrayList<Integer> logIDList;
    @JSONField(name="first_report_time")
    private Long FirstRepoTime;
    @JSONField(name="last_report_time")
    private Long LastRepoTime;
    @JSONField(name="level")
    private int level;
    private Double score;
    private String logger;

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public LogExpEntity(String id, String serviceID) {
        this.id = id;
        this.serviceID = serviceID;
    }

    public LogExpEntity(String id, String content, String serviceID, Long firstRepoTime, String logger) {
        this.id = id;
        this.content = content;
        this.serviceID = serviceID;
        FirstRepoTime = firstRepoTime;
        this.logger = logger;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContext() {
        return content;
    }

    public void setContext(String context) {
        this.content = context;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Integer> getLogIDList() {
        return logIDList;
    }

    public void setLogIDList(ArrayList<Integer> logIDList) {
        this.logIDList = logIDList;
    }

    public Long getFirstRepoTime() {
        return FirstRepoTime;
    }

    public void setFirstRepoTime(Long firstRepoTime) {
        FirstRepoTime = firstRepoTime;
    }

    public Long getLastRepoTime() {
        return LastRepoTime;
    }

    public void setLastRepoTime(Long lastRepoTime) {
        LastRepoTime = lastRepoTime;
    }

    public String getLogger() {
        return logger;
    }

    public void setLogger(String logger) {
        this.logger = logger;
    }
}
