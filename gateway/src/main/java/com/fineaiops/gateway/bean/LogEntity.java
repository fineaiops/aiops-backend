package com.fineaiops.gateway.bean;

import com.alibaba.fastjson.annotation.JSONField;

public class LogEntity {
    @JSONField(name = "id")
    private String id;
    @JSONField(name = "timestamp")
    private long timestamp;
    @JSONField(name = "content")
    private String content;
    @JSONField(name = "type_id")
    private String typeID;

    public LogEntity() {
    }

    public LogEntity(String id, long timestamp, String content, String typeID) {
        this.id = id;
        this.timestamp = timestamp;
        this.content = content;
        this.typeID = typeID;
    }

    public LogEntity(String id, String content) {
        this.id = id;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTypeID() {
        return typeID;
    }

    public void setTypeID(String typeID) {
        this.typeID = typeID;
    }
}
