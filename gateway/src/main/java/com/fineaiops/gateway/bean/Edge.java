package com.fineaiops.gateway.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.sun.org.apache.xpath.internal.operations.Equals;

import java.util.Objects;

public class Edge {
    @JSONField(name = "source")
    private String source;
    @JSONField(name = "target")
    private String target;
    @JSONField(name = "status")
    private int status;

    public Edge(String source, String target) {
        this.source = source;
        this.target = target;
    }

    public Edge(String source, String target, int status) {
        this.source = source;
        this.target = target;
        this.status = status;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;
        Edge edge = (Edge) o;
        return getSource().equals(edge.getSource()) && getTarget().equals(edge.getTarget());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSource(), getTarget());
    }
}
