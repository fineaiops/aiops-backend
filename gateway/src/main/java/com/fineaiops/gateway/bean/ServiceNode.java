package com.fineaiops.gateway.bean;

import com.alibaba.fastjson.annotation.JSONField;

public class ServiceNode {
    @JSONField(name = "id")
    private String id;
    @JSONField(name = "label")
    private String name;
    @JSONField(name = "score")
    private int score;
    @JSONField(name = "status")
    private int nodeType;

    public ServiceNode() {

    }
    public ServiceNode(String id, String name, int nodeType, int score) {
        this.id = id;
        this.name = name;
        this.nodeType = nodeType;
        this.score = score;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNodeType() {
        return nodeType;
    }

    public void setNodeType(int nodeType) {
        this.nodeType = nodeType;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
