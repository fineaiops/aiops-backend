package com.fineaiops.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
        System.out.println(System.getenv("MYSQL_HOST"));
        System.out.println(System.getenv("ES_HOST"));
    }

}
