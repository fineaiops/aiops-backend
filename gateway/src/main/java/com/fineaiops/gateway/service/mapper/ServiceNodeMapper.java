package com.fineaiops.gateway.service.mapper;

import com.fineaiops.gateway.bean.ServiceNode;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ServiceNodeMapper implements RowMapper<ServiceNode> {
    @Override
    public ServiceNode mapRow(ResultSet rs, int rowNum) throws SQLException {
        int isBreak = rs.getInt("is_breakdown");
        int expScore = rs.getInt("exp_score");
        int type = 0;
        if (isBreak > 0) {
            type = 2;
        } else if (expScore > 0) {
            type = 1;
        }
        return new ServiceNode(rs.getString("id"),
                rs.getString("name"),
                type,
                expScore);
    }
}
