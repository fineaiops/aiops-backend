package com.fineaiops.gateway.service;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EsClient {
    private static RestHighLevelClient client;


    @Value("${elasticsearch.host}")
    private String host;
    @Value("${elasticsearch.port}")
    private int port;


    public RestHighLevelClient getClient(){
        if (client != null) {
            return client;
        }
        System.out.println(host+":"+port);
        client = new RestHighLevelClient(
                RestClient.builder(new HttpHost(host, port, "http")));
        return  client;
    }

}
