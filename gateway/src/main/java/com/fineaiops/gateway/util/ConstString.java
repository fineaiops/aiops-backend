package com.fineaiops.gateway.util;

public class ConstString {
    public static final int SUCCESS_DIAGNOSE = 0;
    public static final int SUCCESS_DIAGNOSE_ADD_CATE = 1;
    public static final int SUCCESS_DIAGNOSE_LOG_EXIT = 2;
    public static final int FAILED_DIAGNOSE = -1;
    public static final String exceptionIndex = "log_exception_category";
    public static final String logIndex = "log_collection";
    public static final String metricsDetial = "{\n" +
            "    \"Result\": {\n" +
            "        \"content\": \"ResponseTime\",\n" +
            "        \"id\": 1,\n" +
            "        \"metrics_link\": [\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"ErrorCount\"\n" +
            "                ],\n" +
            "                \"id\": \"bG9jYWxob3N0OjMzMDY=.0\",\n" +
            "                \"ip\": \"10.255.1.1\",\n" +
            "                \"level\": 1,\n" +
            "                \"name\": \"database:3306\",\n" +
            "                \"pr_score\": 0.1967883642544205,\n" +
            "                \"score\": 3.148613828070729,\n" +
            "                \"subject\": false,\n" +
            "                \"type\": 1\n" +
            "            },\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"ErrorCount\",\n" +
            "                    \"QueryPerSecond\"\n" +
            "                ],\n" +
            "                \"id\": \"c3ByaW5nZGVtbw==.1\",\n" +
            "                \"ip\": \"10.255.1.1\",\n" +
            "                \"level\": 2,\n" +
            "                \"name\": \"grpc.kandian.user\",\n" +
            "                \"pr_score\": 0.2003001213931007,\n" +
            "                \"score\": 2.403601456717208,\n" +
            "                \"subject\": true,\n" +
            "                \"type\": 1\n" +
            "            },\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"ErrorCount\",\n" +
            "                    \"QueryPerSecond\"\n" +
            "                ],\n" +
            "                \"id\": \"Zzgoods=.1\",\n" +
            "                \"ip\": \"10.255.1.1\",\n" +
            "                \"level\": 1,\n" +
            "                \"name\": \"grpc.kandian.goods\",\n" +
            "                \"pr_score\": 0.15337700951056624,\n" +
            "                \"score\": 1.2270160760845301,\n" +
            "                \"subject\": false,\n" +
            "                \"type\": 1\n" +
            "            },\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"QueryPerSecond\"\n" +
            "                ],\n" +
            "                \"id\": \"ZzCategory=.1\",\n" +
            "                \"ip\": \"10.255.1.1\",\n" +
            "                \"level\": 0,\n" +
            "                \"name\": \"grpc.kandian.cate\",\n" +
            "                \"pr_score\": 0.049077647561206084,\n" +
            "                \"score\": 0.19631059024482428,\n" +
            "                \"subject\": false,\n" +
            "                \"type\": 1\n" +
            "            }\n" +
            "        ],\n" +
            "        \"service_name\": \"grpc.kandian.user\"\n" +
            "    },\n" +
            "    \"code\": 1,\n" +
            "    \"data\": null,\n" +
            "    \"message\": \"success\"\n" +
            "}";
    public static final String ExceptionMetrics = "{\n" +
            "    \"Result\": {\n" +
            "        \"Exception_categories\": [\n" +
            "            {\n" +
            "                \"content\": \"ResponseTime\",\n" +
            "                \"level\": 0,\n" +
            "                \"id\": 14501,\n" +
            "                \"service_name\": \"grpc.kandian.cate\",\n" +
            "                \"service_ip\": \"192.168.0.177\",\n" +
            "                \"status\": 0\n" +
            "            },\n" +
            "            {\n" +
            "                \"content\": \"QueryPerSecond\",\n" +
            "                \"level\": 0,\n" +
            "                \"id\": 14503,\n" +
            "                \"service_name\": \"grpc.kandian.cate\",\n" +
            "                \"service_ip\": \"192.168.0.177\",\n" +
            "                \"status\": 0\n" +
            "            }\n" +
            "        ]\n" +
            "    },\n" +
            "    \"code\": 1,\n" +
            "    \"data\": null,\n" +
            "    \"message\": \"success\"\n" +
            "}";
    public static final String metricsServiceTopo = "{ "+
            "    \"code\": 1,\n" +
            "    \"data\": {\n" +
            "        \"edges\": [\n" +
            "            {\n" +
            "                \"source\": \"VXNlcg==.0\",\n" +
            "                \"target\": \"c3ByaW5nZGVtbw==.1\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"source\": \"VXNlcg==.0\",\n" +
            "                \"target\": \"ZnJvbnRlbmQ=.1\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"source\": \"ZzCategory=.1\",\n" +
            "                \"target\": \"Zzgoods=.1\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"source\": \"ZzCharts=.1\",\n" +
            "                \"target\": \"Zzgoods=.1\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"source\": \"Zzgoods=.1\",\n" +
            "                \"target\": \"c3ByaW5nZGVtbw==.1\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"source\": \"ZzOrders=.1\",\n" +
            "                \"target\": \"bG9jYWxob3N0OjMzMDY=.0\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"source\": \"ZzOrders=.1\",\n" +
            "                \"target\": \"Zzgoods=.1\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"source\": \"ZzUsers=.1\",\n" +
            "                \"target\": \"bG9jYWxob3N0OjMzMDY=.0\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"source\": \"c3ByaW5nZGVtbw==.1\",\n" +
            "                \"target\": \"bG9jYWxob3N0OjkyMDA=.0\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"source\": \"c3ByaW5nZGVtbw==.1\",\n" +
            "                \"target\": \"bG9jYWxob3N0OjMzMDY=.0\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"nodes\": [\n" +
            "            {\n" +
            "                \"id\": \"bG9jYWxob3N0OjkyMDA=.0\",\n" +
            "                \"level\": 1,\n" +
            "                \"label\": \"database:9200\",\n" +
            "                \"pr_score\": 0.1342087470848044,\n" +
            "                \"score\": 0,\n" +
            "                \"status\": 2\n" +
            "            },\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"ErrorCount\"\n" +
            "                ],\n" +
            "                \"id\": \"bG9jYWxob3N0OjMzMDY=.0\",\n" +
            "                \"level\": 1,\n" +
            "                \"label\": \"database:3306\",\n" +
            "                \"pr_score\": 0.1967883642544205,\n" +
            "                \"score\": 3.148613828070729,\n" +
            "                \"status\": 1\n" +
            "            },\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"ErrorCount\",\n" +
            "                    \"QueryPerSecond\"\n" +
            "                ],\n" +
            "                \"id\": \"c3ByaW5nZGVtbw==.1\",\n" +
            "                \"level\": 2,\n" +
            "                \"label\": \"grpc.kandian.gateway\",\n" +
            "                \"pr_score\": 0.2003001213931007,\n" +
            "                \"score\": 2.403601456717208,\n" +
            "                \"status\": 1\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"VXNlcg==.0\",\n" +
            "                \"level\": 0,\n" +
            "                \"pr_score\": 0.049077647561206084,\n" +
            "                \"score\": 0,\n" +
            "                \"status\": 0\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"ZnJvbnRlbmQ=.1\",\n" +
            "                \"level\": 0,\n" +
            "                \"label\": \"grpc.kandian.frontend\",\n" +
            "                \"pr_score\": 0.06993751995107811,\n" +
            "                \"score\": 0,\n" +
            "                \"status\": 0\n" +
            "            },\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"QueryPerSecond\"\n" +
            "                ],\n" +
            "                \"id\": \"ZzCategory=.1\",\n" +
            "                \"level\": 0,\n" +
            "                \"label\": \"grpc.kandian.cate\",\n" +
            "                \"pr_score\": 0.049077647561206084,\n" +
            "                \"score\": 0.19631059024482428,\n" +
            "                \"status\": 1\n" +
            "            },\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"ErrorCount\"\n" +
            "                ],\n" +
            "                \"id\": \"ZzCharts=.1\",\n" +
            "                \"level\": 0,\n" +
            "                \"label\": \"grpc.kandian.charts\",\n" +
            "                \"pr_score\": 0.049077647561206084,\n" +
            "                \"score\": 0,\n" +
            "                \"status\": 1\n" +
            "            },\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"ErrorCount\",\n" +
            "                    \"QueryPerSecond\"\n" +
            "                ],\n" +
            "                \"id\": \"Zzgoods=.1\",\n" +
            "                \"level\": 1,\n" +
            "                \"label\": \"grpc.kandian.goods\",\n" +
            "                \"pr_score\": 0.15337700951056624,\n" +
            "                \"score\": 1.2270160760845301,\n" +
            "                \"status\": 1\n" +
            "            },\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"QueryPerSecond\"\n" +
            "                ],\n" +
            "                \"id\": \"ZzOrders=.1\",\n" +
            "                \"level\": 0,\n" +
            "                \"label\": \"grpc.kandian.orders\",\n" +
            "                \"pr_score\": 0.049077647561206084,\n" +
            "                \"score\": 0,\n" +
            "                \"status\": 1\n" +
            "            },\n" +
            "            {\n" +
            "                \"anomalous_metrics\": [\n" +
            "                    \"ResponseTime\",\n" +
            "                    \"ErrorCount\"\n" +
            "                ],\n" +
            "                \"id\": \"ZzUsers=.1\",\n" +
            "                \"level\": 0,\n" +
            "                \"label\": \"grpc.kandian.user\",\n" +
            "                \"pr_score\": 0.049077647561206084,\n" +
            "                \"score\": 0,\n" +
            "                \"status\": 1\n" +
            "            }\n" +
            "        ]\n" +
            "    },\n" +
            "    \"message\": \"success\"\n" +
            "}";
    public static final String mockModelList = "{\n" +
            "    \"model_list\": [\n" +
            "        {\n" +
            "            \"model_id\": 10001,\n" +
            "            \"model_name\": \"model_1\",\n" +
            "            \"type\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"model_id\": 10002,\n" +
            "            \"model_name\": \"model_2\",\n" +
            "            \"type\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"model_id\": 10003,\n" +
            "            \"model_name\": \"model_3\",\n" +
            "            \"type\": true\n" +
            "        }\n" +
            "    ],\n" +
            "    \"running_id\": 10001\n" +
            "}";

    public static final String mockResultCode = "{\"result_code\": 0}";


    public static final String mockReviewReport = "{\n" +
            "    \"report_task\": [\n" +
            "        {\n" +
            "            \"task_id\": 1234554,\n" +
            "            \"model_id\": 10002,\n" +
            "            \"start_time\": \"2022-3-14 12:09:02\",\n" +
            "            \"status\": 1\n" +
            "        },\n" +
            "        {\n" +
            "            \"task_id\": 1234555,\n" +
            "            \"model_id\": 10003,\n" +
            "            \"start_time\": \"2022-3-14 13:09:02\",\n" +
            "            \"status\": 0\n" +
            "        },\n" +
            "        {\n" +
            "            \"task_id\": 1234556,\n" +
            "            \"model_id\": 10001,\n" +
            "            \"start_time\": \"2022-3-14 15:09:02\",\n" +
            "            \"status\": 0\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static final String mockReportDetail = "{\n" +
            "    \"report_task_id\": 1234554,\n" +
            "    \"model_id\": 10001,\n" +
            "    \"start_time\": \"2022-3-14 12:09:02\",\n" +
            "    \"dataset_id\": \"dataset1\",\n" +
            "    \"scores\": [\n" +
            "        {\n" +
            "            \"metrics\": \"E\",\n" +
            "            \"score\": 0.4166666666667\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"ACC\",\n" +
            "            \"score\": 0.4166666666667\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"PR\",\n" +
            "            \"score\": 0.4166666666667\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"RE\",\n" +
            "            \"score\": 0.4166666666667\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"F1_score\",\n" +
            "            \"score\": 0.4166666666667\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"Gmean\",\n" +
            "            \"score\": 0.4166666666667\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"TRR\",\n" +
            "            \"score\": 0.4166666666667\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"EPR\",\n" +
            "            \"score\": 0.4166666666667\n" +
            "        }\n" +
            "    ]\n" +
            "}";
    public static final String mockDiffTasks = "{\n" +
            "    \"compare_task\": [\n" +
            "        {\n" +
            "            \"task_id\": 988954,\n" +
            "            \"base_model_id\": 1234554,\n" +
            "            \"compare_model_id\": 1234555,\n" +
            "            \"start_time\": \"2022-3-14 12: 09: 02\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"task_id\": 988955,\n" +
            "            \"base_model_id\": 1234555,\n" +
            "            \"compare_model_id\": 1234556,\n" +
            "            \"start_time\": \"2022-3-14 13: 09: 02\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"task_id\": 988956,\n" +
            "            \"base_model_id\": 1234556,\n" +
            "            \"compare_model_id\": 1234554,\n" +
            "            \"start_time\": \"2022-3-14 15: 09: 02\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static final String mockDiffReport = "{\n" +
            "    \"diff_task_id\": 67858,\n" +
            "    \"base_task_id\": 1234554,\n" +
            "    \"base_model_id\": 10002,\n" +
            "    \"base_dataset_id\": 1,\n" +
            "    \"compare_task_id\": 1234555,\n" +
            "    \"compare_model_id\": 10003,\n" +
            "    \"compare_dataset_id\": 2,\n" +
            "    \"start_time\": \"2022-3-14 12: 09: 02\",\n" +
            "    \"scores\": [\n" +
            "        {\n" +
            "            \"metrics\": \"E\",\n" +
            "            \"base_score\": 0.4166666666667,\n" +
            "            \"compare_score\": 0.4166666666668,\n" +
            "            \"diff_score\": 1e-13\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"ACC\",\n" +
            "            \"base_score\": 0.4166666666667,\n" +
            "            \"compare_score\": 0.4166666666668,\n" +
            "            \"diff_score\": 1e-13\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"PR\",\n" +
            "            \"base_score\": 0.4166666666667,\n" +
            "            \"compare_score\": 0.4166666666668,\n" +
            "            \"diff_score\": 1e-13\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"RE\",\n" +
            "            \"base_score\": 0.4166666666667,\n" +
            "            \"compare_score\": 0.4166666666668,\n" +
            "            \"diff_score\": 1e-13\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"F1_score\",\n" +
            "            \"base_score\": 0.4166666666667,\n" +
            "            \"compare_score\": 0.4166666666668,\n" +
            "            \"diff_score\": 1e-13\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"Gmean\",\n" +
            "            \"base_score\": 0.4166666666667,\n" +
            "            \"compare_score\": 0.4166666666668,\n" +
            "            \"diff_score\": 1e-13\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"TRR\",\n" +
            "            \"base_score\": 0.4166666666667,\n" +
            "            \"compare_score\": 0.4166666666668,\n" +
            "            \"diff_score\": 1e-13\n" +
            "        },\n" +
            "        {\n" +
            "            \"metrics\": \"EPR\",\n" +
            "            \"base_score\": 0.4166666666667,\n" +
            "            \"compare_score\": 0.4166666666668,\n" +
            "            \"diff_score\": 1e-13\n" +
            "        }\n" +
            "    ],\n" +
            "    \"conclude\": \"一些建议\"\n" +
            "}";
    public static final String mockAllReportIDs = "{\n" +
            "    \"report_task_ids\": [\n" +
            "        1234554,\n" +
            "        1234555,\n" +
            "        1234556\n" +
            "    ]\n" +
            "}";
    public static String getAccessOrigin() {
        String host = System.getenv("ACCESS_HOST");
        String port = System.getenv("ACCESS_PORT");
        if (host.isEmpty()) {
            host = "localhost";
        }
        if (port.isEmpty()) {
            port = "8080";
        }
        return "http://" + host + ":" + port;
    }
}
