package com.fineaiops.gateway.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CSVUtil {
    public static void convert(){
        try {
            //  读取文件 “GBK”解决文件里中文乱码
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("/Users/jialichun/本地文稿/dataset/error.csv"), "GBK"));
            String line ;
            int i = 1;
            Set<String> server = new HashSet<>();
            Set<String> all = new HashSet<>();
            while ((line = reader.readLine()) != null) {
                // 分割字符串
                String[] item = line.split(",");
                all.add(item[1]);
                if (item.length >4 && item[4].endsWith("Server")) {
                    // 调用下面的方法，将每列数据赋值到对应的属性中，返回的结果新增到list内
                    server.add(item[4]);
                }
//                System.out.println(Arrays.toString(item));
                i++;
            }
            System.out.println("sum line count: "+i);
            System.out.println("sum server: "+ server.size());
            System.out.println("sum line: "+ i);
            System.out.println(server);
//            System.out.println(all);
            // 保存到数据库
//            insertDB(result);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void convert2(){
        try {
            //  读取文件 “GBK”解决文件里中文乱码
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("/Users/jialichun/本地文稿/dataset/training_data_with_faults/tar/cloudbed-1/log/all/log_filebeat-testbed-log-service.csv"), "GBK"));
            String line ;
            int i = 1;
//            Set<String> server = new HashSet<>();
            Set<String> all = new HashSet<>();
            Set<String> whiteServer = new HashSet<>(Arrays.asList("checkoutservice,cartservice,currencyservice,frontend,recommendationservice".split(",")));
            while ((line = reader.readLine()) != null) {
                i++;
                // 分割字符串
                String[] item = line.split(",");
//                all.add(item[2].split("-")[0]);
                if (item.length < 5 || !whiteServer.contains(item[2].split("-")[0])) {
                    continue;
                }
                String msg = item[4];
                for (int k = 4; k < item.length; k++) {
                    msg = msg+item[k];
                }
                boolean flag = false;
                all.add(item[0]);
//                if (item.length >4 && item[4].endsWith("Server")) {
//                    // 调用下面的方法，将每列数据赋值到对应的属性中，返回的结果新增到list内
//                    server.add(item[4]);
//                }
//                System.out.println(Arrays.toString(item));
            }
            System.out.println("sum line count: "+i);
            System.out.println("sum white: "+ all.size());
//            System.out.println(server);
//            System.out.println(all);
            // 保存到数据库
//            insertDB(result);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
//        convert();
        convert2();
    }
}
