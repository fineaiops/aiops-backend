package com.fineaiops.gateway.util;

import com.alibaba.fastjson.JSONObject;

public class JsonBuilder {
    JSONObject object;
    public JsonBuilder() {
        object = new JSONObject();
    }
    public JsonBuilder put(String key, Object value) {
        object.put(key,value);
        return this;
    }
    public JSONObject build() {
        return object;
    }
}
