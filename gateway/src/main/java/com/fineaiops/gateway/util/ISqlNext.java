package com.fineaiops.gateway.util;

import java.sql.ResultSet;

public interface ISqlNext {
    public void next(ResultSet rs);
}