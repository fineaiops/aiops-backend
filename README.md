# AIOPS后端

#### 介绍
AIOPS项目旨在微服务系统生态运维，主要功能包括微服务链路分析、故障诊断、异常报警和根因追踪。
#### 软件架构
软件架构说明


#### 安装教程
##### 方法1 dockerhub下载镜像部署
1. 安装环境：确保服务器已安装docker
2. 自动从dockerhub拉取镜像并运行镜像实例，-e 需要的参数请看使用说明1
```
docker run --name gateway --network {docker网络(可选)} -p 8090:8090 -e MYSQL_HOST={mysql主机地址} -e ES_HOST={es主机地址} darkone0/aiopsgateway
```
3. 访问 `http://{服务器ip地址}:8090/test`, 预期结果返回`hello, world`
##### 方法2 源码编译镜像部署
1. 安装环境：确保服务器已安装docker 和 maven 项目源码拷贝到服务器
2. 镜像构建：进入项目源码根目录，运行gateway/build.sh
```
sh gateway/build.sh
```
3. 运行实例，-e 需要的参数请看使用说明1
```
docker run --name gateway --network {docker网络(可选)} -p 8090:8090 -e MYSQL_HOST={mysql主机地址} -e ES_HOST={es主机地址} aiopsgateway
```
4. 访问 `http://{服务器ip地址}:8090/test`, 预期结果返回`hello, world`。如果在本地部署，默认情况下ip地址为localhost或127.0.0.1。

##### 方法选择建议
方法1基于稳定镜像构建，更加稳定简捷，适合于线上服务器部署；\
方法2基于源码构建，适合本地部署和代码调试。

#### 使用说明

1. 容器启动需要配置的环境变量参数
```
docker run darkone0/gateway \
    -e MYSQL_HOST={mysql 服务器地址} \
    -e MYSQL_PORT={mysql 服务器地址} \
    -e MYSQL_PASSWORD={mysql 服务器地址} \
    -e ES_HOST={elasticsearch 服务器地址} \
    -e ES_PORT={elasticsearch 服务器地址} \
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

