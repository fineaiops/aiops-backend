from app import db


class DependencyGraph(db.Model):
    __tablename__ = 'dependency_graph'
    id = db.Column(db.Integer, primary_key=True)
    graph = db.Column(db.JSON)

    def toJson(self):
        return {'graph': self.graph}

    def to_dict(self):
        return {c.name: getattr(self, c.name, None) for c in self.__table__.columns}


class Service(db.Model):
    __tablename__ = 'services'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    host = db.Column(db.String)
    container = db.Column(db.String)
    influence = db.Column(db.Float)
    possibility = db.Column(db.Float)

    def to_dict(self):
        return {c.name: getattr(self, c.name, None) for c in self.__table__.columns}


class AnomalousMetric(db.Model):
    __tablename__ = 'anomalous_metrics'
    id = db.Column(db.Integer, primary_key=True)
    serviceName = db.Column(db.String)
    type = db.Column(db.String)
    time = db.Column(db.String)

    def to_dict(self):
        return {c.name: getattr(self, c.name, None) for c in self.__table__.columns}


class AnomalyPropagationChain(db.Model):
    __tablename__ = 'anomaly_propagation_chains'
    id = db.Column(db.Integer, primary_key=True)
    chain = db.Column(db.String)

    def to_dict(self):
        return {c.name: getattr(self, c.name, None) for c in self.__table__.columns}

