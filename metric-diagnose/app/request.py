from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField


class RequestForm(FlaskForm):
    dependency_graph = StringField('服务依赖图')
    dependency_path = StringField('服务依赖链')
    submit = SubmitField('提交请求')
