import networkx as nx


def calculate_pr(graph):
    nodes = graph['nodes']
    edges = graph['edges']
    g = nx.DiGraph()
    g.add_nodes_from([item['id'] for item in nodes])
    g.add_edges_from([(item['source'], item['target']) for item in edges])
    scores = nx.pagerank(g, alpha=0.85, max_iter=100, tol=0.00001)
    for item in nodes:
        item['score'] = scores[item['id']]
        if item['score'] < 1 / len(nodes):
            item['level'] = 0
        elif item['score'] < 2 / len(nodes):
            item['level'] = 1
        else:
            item['level'] = 2
    return graph
