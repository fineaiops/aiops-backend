from app import db
from app.models import DependencyGraph


def insertGraph(nodes, edges):
    g = DependencyGraph(graph={'nodes': nodes, 'edges': edges})
    db.session.add(g)
    db.session.commit()
