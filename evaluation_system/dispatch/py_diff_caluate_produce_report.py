#!/usr/bin/python3
import sys
import math

html_style = """
<style type="text/css">
table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
table.tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
table.tftable tr {background-color:#ffffff;}
table.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}
</style>
"""
html_body = """
<head>
<meta charset="utf-8" />
</head>

<table id="tfhover" class="tftable" border="1">
<tr><th colspan="4">report: %s</th></tr>
<tr><th colspan="4">base_report: %s</th></tr>
<tr><th colspan="4">compare_report: %s</th></tr>
<tr><td>metrics</td><td>base_report</td><td>compare_report</td><td>Diff</td></tr>
<tr><td>E</td><td>%s</td><td>%s</td><td>%s</td></tr>
<tr><td>Acc</td><td>%s</td><td>%s</td><td>%s</td></tr>
<tr><td>Pr</td><td>%s</td><td>%s</td><td>%s</td></tr>
<tr><td>Re</td><td>%s</td><td>%s</td><td>%s</td></tr>
<tr><td>F1_Score</td><td>%s</td><td>%s</td><td>%s</td></tr>
<tr><td>Gmean</td><td>%s</td><td>%s</td><td>%s</td></tr>
<tr><td>TPR</td><td>%s</td><td>%s</td><td>%s</td></tr>
<tr><td>FPR</td><td>%s</td><td>%s</td><td>%s</td></tr>
<tr><td rowspan="2">比较分析</td><td colspan="3" rowspan="2">%s<br/>%s</td></tr>
</table>

"""

if __name__ == '__main__':
    result_string_1 = ""
    with open("/datas/tmp/test_result_1.txt", "r") as f:
        result_string_1 = f.read()
    result_string_2 = ""
    with open("/datas/tmp/test_result_2.txt", "r") as f:
        result_string_2 = f.read()
    base_path = sys.argv[1]
    compare_path = sys.argv[2]
    output_path = sys.argv[3]
    print(result_string_1)
    B_TN = 1
    B_FN = 1
    B_TP = 1
    B_FP = 1
    result_elements = result_string_1.split(' ')
    
    for i in range(len(result_elements)):
        if result_elements[i] == "TN":
            B_TN += int(result_elements[i + 1])
        if result_elements[i] == "FN":
            B_FN += int(result_elements[i + 1])
        if result_elements[i] == "TP":
            B_TP += int(result_elements[i + 1])
        if result_elements[i] == "FP":
            B_FP += int(result_elements[i + 1])
    print("B_TN: ",B_TN)
    print("B_FN: ",B_FN)
    print("B_TP: ",B_TP)
    print("B_FP: ",B_FP)

    B_E = (1.0* (B_FN + B_FP) )/(B_TP + B_FN + B_FP + B_TN)
    B_ACC = (1.0* (B_TN + B_TP))/(B_TP + B_FN + B_FP + B_TN)
    B_Pr = (1.0* B_TP )/(B_TP + B_FP)
    B_Re = (1.0* B_TP )/(B_TP + B_FN)
    B_F1_Score = (2 * B_Pr * B_Re) / (B_Pr + B_Re)
    B_Gmean = math.sqrt((B_TP/(B_TP+B_FN))* (B_TN/(B_TN+B_FP)))
    B_TPR = (1.0 * B_TP )/(B_TP + B_FN)
    B_FPR = (1.0 * B_FP )/(B_FP + B_TN)

    print("E: ",B_E)
    print("ACC: ",B_ACC)
    print("Pr: ",B_Pr)
    print("Re: ",B_Re)

    print(result_string_2)
    C_TN = 1
    C_FN = 1
    C_TP = 1
    C_FP = 1
    result_elements = result_string_2.split(' ')
    
    for i in range(len(result_elements)):
        if result_elements[i] == "TN":
            C_TN += int(result_elements[i + 1])
        if result_elements[i] == "FN":
            C_FN += int(result_elements[i + 1])
        if result_elements[i] == "TP":
            C_TP += int(result_elements[i + 1])
        if result_elements[i] == "FP":
            C_FP += int(result_elements[i + 1])
    print("C_TN: ",C_TN)
    print("C_FN: ",C_FN)
    print("C_TP: ",C_TP)
    print("C_FP: ",C_FP)

    C_E = (1.0* (C_FN + C_FP) )/(C_TP + C_FN + C_FP + C_TN)
    C_ACC = (1.0* (C_TN + C_TP))/(C_TP + C_FN + C_FP + C_TN)
    C_Pr = (1.0* C_TP )/(C_TP + C_FP)
    C_Re = (1.0* C_TP )/(C_TP + C_FN)
    C_F1_Score = (2 * C_Pr * C_Re) / (C_Pr + C_Re)
    C_Gmean = math.sqrt((C_TP/(C_TP+C_FN))* (C_TN/(C_TN+C_FP)))
    C_TPR = (1.0 * C_TP )/(C_TP + C_FN)
    C_FPR = (1.0 * C_FP )/(C_FP + C_TN)

    print("E: ",C_E)
    print("ACC: ",C_ACC)
    print("Pr: ",C_Pr)
    print("Re: ",C_Re)

    Diff_TN = B_TN - C_TN
    Diff_FN = B_FN - C_FN
    Diff_TP = B_TP - C_TP
    Diff_FP = B_FP - C_FP

    Diff_E = B_E - C_E
    Diff_ACC = B_ACC - C_ACC
    Diff_Pr = B_Pr - C_Pr
    Diff_Re = B_Re - C_Re
    Diff_F1_Score = B_F1_Score -C_F1_Score
    Diff_Gmean = B_Gmean - C_Gmean
    Diff_TPR = B_TPR - C_TPR
    Diff_FPR = B_FPR - C_FPR

    analysis1 = ""
    analysis2 = ""
    ana1 = ['被比较模型的错误率(E)、分类准确率(ACC)、精度(Precision)、查全率(Recall)等指标综合表现更好。',
        '被比较模型的错误率(E)、分类准确率(ACC)、精度(Precision)、查全率(Recall)等指标综合表现不如比较模型，建议用户通过标准化/归一化、正则化、添加dropout层来尝试优化。']
    ana2 = ['被比较模型对正常日志判断准确率高，对异常日志判断准确率低，可能存在数据不平衡的问题，建议用户通过过采样、欠采样、样本加权等方式进行优化。',
        '被比较模型对正常日志和异常日志判断准确率都较高，不存在数据不平衡的问题。']
    
    if Diff_ACC + Diff_Pr + Diff_Re - Diff_E > 0:
        analysis1 = ana1[0]
    else:
        analysis1 = ana1[1]
    
    if B_FPR <0.2 and B_TPR < 0.6:
        analysis2 = ana2[0]
    else:
        analysis2 = ana2[1]

    report_name = output_path.replace("/", "_")
    report_name = "/server/templates/" + report_name + ".html"

    # print(output_path)
    # print(TN)
    # print(FN)
    # print(TP)
    # print(FP)
    # print(E)
    # print(ACC)
    # print(Pr)
    # print(Re)
    report = html_style + html_body % (output_path, base_path, compare_path, 
        B_E, C_E, Diff_E,
        B_ACC, C_ACC, Diff_ACC,
        B_Pr, C_Pr, Diff_Pr,
        B_Re, C_Re, Diff_Re,
        B_F1_Score, C_F1_Score, Diff_F1_Score,
        B_Gmean, C_Gmean, Diff_Gmean,
        B_TPR, C_TPR, Diff_TPR,
        B_FPR, C_FPR, Diff_FPR,
        analysis1, analysis2)
    
    print(report)
    report_file = open(report_name, "w")
    
    report_file.write(report)
    report_file.close()
