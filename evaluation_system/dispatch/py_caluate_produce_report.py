#!/usr/bin/python3
import sys
import math

html_style = """
<style type="text/css">
table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
table.tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
table.tftable tr {background-color:#ffffff;}
table.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}
</style>
"""
html_body = """

<table id="tfhover" class="tftable" border="1">
<tr><th colspan="2">%s</th></tr>
<tr><td>E</td><td>%s</td></tr>
<tr><td>Acc</td><td>%s</td></tr>
<tr><td>Pr</td><td>%s</td></tr>
<tr><td>Re</td><td>%s</td></tr>
<tr><td>F1_Score</td><td>%s</td></tr>
<tr><td>Gmean</td><td>%s</td></tr>
<tr><td>TPR</td><td>%s</td></tr>
<tr><td>FPR</td><td>%s</td></tr>
</table>

"""

if __name__ == '__main__':
    result_string = ""
    with open("/datas/tmp/test_result.txt", "r") as f:
        result_string = f.read()
    output_path = sys.argv[1]
    print(result_string)
    TN = 0
    FN = 0
    TP = 0
    FP = 0
   
    result_elements = result_string.split(' ')

    for i in range(len(result_elements)):
        if result_elements[i] == "TN":
            TN += int(result_elements[i + 1])
        if result_elements[i] == "FN":
            FN += int(result_elements[i + 1])
        if result_elements[i] == "TP":
            TP += int(result_elements[i + 1])
        if result_elements[i] == "FP":
            FP += int(result_elements[i + 1])
    
    TN = TN + 1
    FN = FN + 1
    TP = TP + 1
    FP = FP + 1
    print("TN: ",TN)
    print("FN: ",FN)
    print("TP: ",TP)
    print("FP: ",FP)

    E = (1.0* (FN + FP) )/(TP + FN + FP + TN)
    ACC = (1.0* (TN + TP))/(TP + FN + FP + TN)
    Pr = (1.0* TP )/(TP + FP)
    Re = (1.0* TP )/(TP + FN)
    F1_Score = (2 * Pr * Re) / (Pr + Re)
    Gmean = math.sqrt((TP/(TP+FN))* (TN/(TN+FP)))
    TPR = (1.0 * TP )/(TP + FN)
    FPR = (1.0 * FP )/(FP + TN)

    print("E: ",E)
    print("ACC: ",ACC)
    print("Pr: ",Pr)
    print("Re: ",Re)

    report_name = output_path.replace("/", "_")
    report_name = "/server/templates/" + report_name + ".html"

    print(output_path)
    print(TN)
    print(FN)
    print(TP)
    print(FP)
    print(E)
    print(ACC)
    print(Pr)
    print(Re)
    report = html_style + html_body % (output_path, E, ACC, Pr, Re, F1_Score, Gmean, TPR,FPR)
    
    print(report)
    report_file = open(report_name, "w")
    
    report_file.write(report)
    report_file.close()
