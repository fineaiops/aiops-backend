#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
from loglizer.models import LR
from loglizer import dataloader, preprocessing

import joblib
import numpy as np


data = '../data/loglizer_data/loglizer_data.txt'

if __name__ == '__main__':
    line = None
    with open(data, 'r') as f:
        line = f.readline()
    
    line = line.strip('/n')
    print(line)
    line_elements = line.split(',')
    print(line_elements)
    x_list = []
    for i in range(14):
        x_list.append(float(line_elements[i]))
    y = int(line_elements[14])
    print(y)


    model = joblib.load('../saved_models/LR_1129.model')

    predict_label = model.predict(np.array(x_list).reshape(1, -1))
    print(predict_label)
    print(y)