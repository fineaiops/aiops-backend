#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('/datas/loglizer/')
from loglizer.models import DecisionTree
from loglizer import dataloader, preprocessing

import joblib
import numpy as np

data = '../data/loglizer_data/loglizer_data.txt'

if __name__ == '__main__':
    #print(sys.argv[0])          #sys.argv[0] 类似于shell中的$0,但不是脚本名称，而是脚本的路径   
    #print(sys.argv[1])
    
    line = sys.argv[1]
    #print(line)
    line_elements = line.split(',')
    #print(line_elements)
    x_list = []
    for i in range(14):
        x_list.append(line_elements[i])
    y = int(line_elements[14])
    #print(y)

    model = joblib.load('/datas/loglizer/saved_models/DecisionTree_0111.model')
    
    predict_label = model.predict(np.array(x_list).reshape(1, -1))
    #print(predict_label)
    #print(y)

    if(predict_label[0] == 1 and y == 1):
        print("TP")
    elif(predict_label[0] == 0 and y == 0):
        print("TN")
    elif(predict_label[0] == 0 and y == 0):
        print("FP")
    else:
        print("FN")
    
