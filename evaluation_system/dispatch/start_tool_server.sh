#!/bin/bash
# author: gaozirui

Model=""
Dataset=""
Output_path=""

while getopts "m:d:o:h" opt; do
    case $opt in
    m)
        Model=$OPTARG
        ;;
    d)
        Dataset=$OPTARG
        ;;
    o)
        Output_path=$OPTARG
        ;;
    h)
        echo "-h: 查阅参数功能"
        echo "-m modelid: 输入modelid"
        echo "-d datasetid: 输入datasetid"
        echo "-o Output_path: 输入Output_path"
        echo "目前支持的数据集有dataset_1, dataset_2, dataset_3"
        echo "目前支持的日志异常检测模型有 Lr, DecisionTree, Svm"
        exit 3
        ;;
    :)
        echo "选项-$OPTARG后面需要一个参数值"
        exit 1
        ;;
    ?)
        echo "无效的选项 -$OPTARG"
        exit 2
        ;;
    esac
done
echo $(date +"%Y-%m-%d %H:%M:%S")" 评测任务开始"
echo $(date +"%Y-%m-%d %H:%M:%S")" 根据数据集id查找子数据集文件"

Index_file=`python3 /datas/py_mysql.py "select index_file_contain from dataset where id = '${Dataset}'"`

echo $(date +"%Y-%m-%d %H:%M:%S")" 调度子数据文件数量的集群机器进行分布式评测"

Hadoop_cmd="hadoop jar /datas/mapreduce01-1.0-SNAPSHOT.jar com.myhadoop."$Model"Hadoop "$Index_file" "$Output_path
echo $Hadoop_cmd
$Hadoop_cmd

echo $(date +"%Y-%m-%d %H:%M:%S")" 评测结束，计算生成评测报告"
result_path=$Output_path"/part-r-00000"
#result=` hadoop dfs -cat $result_path `
#echo $result > /datas/tmp/test_result.txt

#python3 /datas/py_caluate_produce_report.py $Output_path

#echo $(date +"%Y-%m-%d %H:%M:%S")" 评测任务结束"

