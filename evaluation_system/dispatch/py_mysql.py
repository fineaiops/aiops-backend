#!/usr/bin/python3
import sys
import pymysql


if __name__ == '__main__':
    sql_cmd = sys.argv[1]
    # 打开数据库连接
    db = pymysql.connect(host='',
                        port=3111,
                        user='',
                        password='',
                        database='')
    
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()
    
    # 使用 execute()  方法执行 SQL 查询 
    cursor.execute(sql_cmd)
    
    # 使用 fetchone() 方法获取单条数据.
    data = cursor.fetchone()

    print (data[0])
    with open("/datas/tmp/sql_result.txt", "w") as f:
        f.write(data[0])

    # 关闭数据库连接
    db.close()
