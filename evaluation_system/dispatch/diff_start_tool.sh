#!/bin/bash
# author: gaozirui

Model=""
report_id_1=""
report_id_2=""
Output_path=""

while getopts "m:b:c:o:h" opt; do
    case $opt in
    m)
        Model=$OPTARG
        ;;
    b)
        report_id_1=$OPTARG
        ;;
    c)
        report_id_2=$OPTARG
        ;;
    o)
        Output_path=$OPTARG
        ;;
    h)
        echo "-h: 查阅参数功能"
        echo "-m modelid: 输入modelid"
        echo "-b report_id_1: 输入base_report_id(report_id_1) "
        echo "-c report_id_2: 输入compare_report_id(report_id_2)"
        echo "-o Output_path: 输入Output_path"
        echo "目前支持的数据集有dataset_1, dataset_2, dataset_3"
        echo "目前支持的日志异常检测模型有 Lr, DecisionTree, SVM"
        exit 3
        ;;
    :)
        echo "选项-$OPTARG后面需要一个参数值"
        exit 1
        ;;
    ?)
        echo "无效的选项 -$OPTARG"
        exit 2
        ;;
    esac
done

echo $(date +"%Y-%m-%d %H:%M:%S")" diff评测任务开始"
echo $(date +"%Y-%m-%d %H:%M:%S")" 根据数report_id查找report数据"

result_path_1=$report_id_1"/part-r-00000"
result_1=` hadoop dfs -cat $result_path_1 `
echo $result_1 > /datas/tmp/test_result_1.txt

result_path_2=$report_id_2"/part-r-00000"
result_2=` hadoop dfs -cat $result_path_2 `
echo $result_2 > /datas/tmp/test_result_2.txt

python3 /datas/py_diff_caluate_produce_report.py $report_id_1 $report_id_2 $Output_path
echo $(date +"%Y-%m-%d %H:%M:%S")" 评测任务结束"