package com.myhadoop;

/**
 * 项目名称：MapReduce01
 * 类 名 称：MyReducer
 * 类 描 述：TODO
 * 创建时间：2021/11/23 上午9:03
 * 创 建 人：allen
 * e-mail ：allengao@pku.edu.cn
 */
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;

public class MyReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
    private IntWritable v = new IntWritable();

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        for (IntWritable value : values) {
            sum += value.get();
        }
        v.set(sum);
        context.write(key,v);
    }
}