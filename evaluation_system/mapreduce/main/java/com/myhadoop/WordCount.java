package com.myhadoop;

/**
 * 项目名称：MapReduce01
 * 类 名 称：WordCount
 * 类 描 述：TODO
 * 创建时间：2021/11/23 上午9:02
 * 创 建 人：allen
 * e-mail ：allengao@pku.edu.cn
 */
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import java.io.IOException;

public class WordCount {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        //构建Configuration实例
        Configuration configuration = new Configuration();
        //其他配置信息

        //获得Job实例
        Job job = Job.getInstance(configuration,"My WordCount Job");
        job.setJarByClass(WordCount.class);

        //设置Mapper和Reducer处理类
        job.setMapperClass(MyMapper.class);
        job.setReducerClass(MyReducer.class);

        //设置Mapper和Reducer的输入输出格式
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        //设置输出结果的数据格式
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        //指定输入和输出路径
        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        //提交任务，true为提交成功，如果为true打印0，为false打印1
        boolean b = job.waitForCompletion(true);
        System.exit(b ? 0 : 1);

    }
}