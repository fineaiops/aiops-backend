package com.myhadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * 项目名称：MapReduce01
 * 类 名 称：LoglizerHadoop
 * 类 描 述：TODO
 * 创建时间：2021/12/6 下午3:31
 * 创 建 人：allen
 * e-mail ：allengao@pku.edu.cn
 */

public class LrHadoop {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        //构建Configuration实例
        Configuration configuration = new Configuration();
        //其他配置信息

        //获得Job实例
        Job job = Job.getInstance(configuration,"Loglizer");
        job.setJarByClass(LrHadoop.class);

        //设置Mapper和Reducer处理类
        job.setMapperClass(LrMapper.class);
        job.setReducerClass(MyReducer.class);

        //设置Mapper和Reducer的输入输出格式
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        //设置输出结果的数据格式
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        //指定输入和输出路径
        //job.setInputFormatClass(SplitByLine.class);
        String[] inputPathStr = args[0].split(",");
        Path[] inPaths = new Path[inputPathStr.length];
        int i = 0;
        for(String inPathStr:inputPathStr){
            inPaths[i++] = new Path(inPathStr);
        }
        FileInputFormat.setInputPaths(job, inPaths);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        //提交任务，true为提交成功，如果为true打印0，为false打印1
        boolean b = job.waitForCompletion(true);
        System.exit(b ? 0 : 1);

    }
}
