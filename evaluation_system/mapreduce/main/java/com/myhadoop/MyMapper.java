package com.myhadoop;

/**
 * 项目名称：MapReduce01
 * 类 名 称：MyMapper
 * 类 描 述：TODO
 * 创建时间：2021/11/23 上午9:04
 * 创 建 人：allen
 * e-mail ：allengao@pku.edu.cn
 */
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<LongWritable, Text, Text, IntWritable>{
    private Text k = new Text();
    private IntWritable v = new IntWritable(1);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] words = value.toString().split(",");

        //模拟简单分类模型
        //是否是奇数

        //inference
        double random = Math.random();
        int t = Integer.parseInt(words[0]);
        int label = Integer.parseInt(words[1]);
        int predict_label = 0;
        if(random < 0.3){
            predict_label = (t + 1) % 2;
        }
        else{
            predict_label = t % 2;
        }

        //evaluate
        if(predict_label == 1 & label == 1){
            context.write(new Text("TP"), new IntWritable(1));
        }
        else if(predict_label == 0 & label == 0){
            context.write(new Text("TN"), new IntWritable(1));
        }
        else if(predict_label == 1 & label == 0){
            context.write(new Text("FP"), new IntWritable(1));
        }
        else{
            context.write(new Text("FN"), new IntWritable(1));
        }
    }
}