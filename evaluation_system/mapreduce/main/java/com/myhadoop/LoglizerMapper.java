package com.myhadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 项目名称：MapReduce01
 * 类 名 称：LoglizerMapper
 * 类 描 述：TODO
 * 创建时间：2021/12/6 下午3:28
 * 创 建 人：allen
 * e-mail ：allengao@pku.edu.cn
 */


public class LoglizerMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    private Text k = new Text();
    private IntWritable v = new IntWritable(1);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String data_label = value.toString();

        System.out.println("data_label: ");
        System.out.println(data_label);

        //调用python
        String pyPath = "/datas/loglizer/load_predict/load_predict.py";
        String[] args = new String[] {"python3", pyPath, data_label};

        System.out.println("pyPath: ");
        System.out.println(pyPath);

        // 执行Python文件，并传入参数
        Process proc = Runtime.getRuntime().exec(args);

        System.out.println("1!");
        // 获取Python输出字符串作为输入流被Java读取
        BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));

        System.out.println("2!");

        String actionStr = in.readLine();

        in.close();
        proc.waitFor();

        System.out.println("lastLine: ");
        System.out.println(actionStr);
//        System.out.println(lastLine);
        //获取最后的输出，是TP FP TN Fp
        context.write(new Text(actionStr), new IntWritable(1));
    }
}
