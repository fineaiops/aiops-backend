from flask import Flask,send_file,request,jsonify
import pymysql
pymysql.install_as_MySQLdb()
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI']=''

db = SQLAlchemy(app)

class Evaluate_task(db.Model):
    __tablename__ = 'evaluate_task'
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    model_id = db.Column(db.Integer)
    start_time = db.Column(db.String(64))
    dataset_id = db.Column(db.Integer)
    status = db.Column(db.Integer)

    def __repr__(self):
        return 'Evaluate_task(%r)' % self.id

class Compare_task(db.Model):
    __tablename__ = 'compare_task'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    base_model_id = db.Column(db.Integer)
    compare_model_id = db.Column(db.Integer)
    task_id_0 = db.Column(db.Integer)
    task_id_1 = db.Column(db.Integer)
    start_time = db.Column(db.String(64))

    def __repr__(self):
        return 'Compare_task(%r)' % self.id

class Datasets(db.Model):
    __tablename__ = 'datasets'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    dataset_name = db.Column(db.String(64))
    create_time = db.Column(db.String(64))

    def __repr__(self):
        return 'dataset(%r)' % self.id


if __name__ == '__main__':
    db.drop_all()
    db.create_all()