#!/bin/bash
# author: gaozirui

report_id_1=$1
report_id_2=$2

result_path_1=$report_id_1
result_1=` hadoop dfs -cat $result_path_1 `
echo $result_1 > /datas/tmp/test_result_1.txt

result_path_2=$report_id_2
result_2=` hadoop dfs -cat $result_path_2 `
echo $result_2 > /datas/tmp/test_result_2.txt